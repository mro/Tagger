open Alcotest

(* Build with `ocamlbuild -pkg alcotest simple.byte` *)

(* A module with functions to test *)
module To_test = struct
  let lowercase = String.lowercase_ascii

  let capitalize = String.capitalize_ascii

  let str_concat = String.concat ""

  let list_concat = List.append
end

(* The tests *)
let tc_lowercase () =
  (check string) "same string" "hello!" (To_test.lowercase "hELLO!")

let tc_capitalize () =
  (check string) "same string" "World." (To_test.capitalize "world.")

let tc_str_concat () =
  (check string)
    "same string" "foobar"
    (To_test.str_concat [ "foo"; "bar" ])

let tc_list_concat () =
  (check (list int))
    "same lists" [ 1; 2; 3 ]
    (To_test.list_concat [ 1 ] [ 2; 3 ])

(* Run it *)
let () =
  run "Utils" [
    __FILE__ , [
      "tc_lowercase"  , `Quick, tc_lowercase;
      "tc_capitalize" , `Quick, tc_capitalize;
      "tc_str_concat" , `Quick, tc_str_concat;
      "tc_list_concat", `Quick, tc_list_concat;
    ]
  ] 
