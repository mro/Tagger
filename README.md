
Add, delete and list tags of files stored in filenames, inspired by
https://karl-voit.at/managing-digital-photographs/

## Synopsis

```sh
$ meta tag lst [file]
$ meta tag add <tag> [file]
$ meta tag del <tag> [file]
$ meta title set <title> [file]
$ meta title get [file]
```


## Design Goals

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |   ×  |        |            |
| Reliability     |      ×    |      |        |            |
| Usability       |           |   ×  |        |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |           |   ×  |        |            |
| Portability     |           |      |    ×   |            |


## Mirrors

see doap.rdf
