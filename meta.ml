(* *)

let err i msgs =
  let exe = Filename.basename Sys.executable_name in
  msgs |> List.cons exe |> String.concat ": " |> prerr_endline;
  i

let file_rename oc f p' =
  let f' = Name.unparse p' in
  (* https://ocaml.github.io/ocamlunix/ocamlunix.html#sec13 *)
  match Sys.file_exists f' with
  | true -> err 3 [ f'; "file exists" ]
  | false -> (
      try
        Sys.rename f f';
        Printf.fprintf oc "%s --> %s\n" f f';
        0
      with Sys_error e -> err 4 [ f; e ])

let title_get oc files =
  files
  |> List.map (fun f ->
      let _, _, title, _, _ = Name.parse f in
      title)
  |> List.iter (fun (Name.Title tv) -> Printf.fprintf oc "%s\n" tv);
  0

let title_set oc title f =
  let dir, datetime, _, tags, exts = Name.parse f in
  (dir, datetime, title, tags, exts) |> file_rename oc f

let () =
  let print_help oc =
    let msg =
      "Add, delete and list tags from filenames, inspired by \
       https://karl-voit.at/managing-digital-photographs/\n\n\
       SYNOPSIS\n\n\
      \  $ meta -h                # get this help\n\
      \  $ meta -V                # version\n\n\
      \  $ meta tag lst [file]\n\
      \  $ meta tag add <tag> [file]\n\
      \  $ meta tag del <tag> [file]\n\n\
      \  $ meta title get [file]\n\
      \  $ meta title set <title> [file]\n\n\
       EXAMPLE\n\n\
      \  $ meta tag lst * | sort | uniq -c\n"
    in
    Printf.fprintf oc "%s\n" msg;
    0
  and print_version oc =
    let exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc "%s: https://mro.name/%s/v%s\n" exe "meta"
      Version.git_sha;
    0
  and tag_add oc tag f = tag |> Name.tag_add (Name.parse f) |> file_rename oc f
  and tag_del oc tag f = tag |> Name.tag_del (Name.parse f) |> file_rename oc f
  and tag_lst oc files =
    files
    |> List.map (fun f ->
        let _, _, _, tags, _ = Name.parse f in
        tags)
    |> List.concat
    |> List.iter (fun (Name.Tag tv) -> Printf.fprintf oc "%s\n" tv);
    0
  and each v fkt lst =
    lst |> List.iter (fun x -> ignore (fkt v x));
    0
  in
  (match Sys.argv |> Array.to_list |> List.tl with
   | [ "-h" ] | [ "--help" ] -> print_help stdout
   | [ "-V" ] | [ "--version" ] -> print_version stdout
   | "tag" :: "lst" :: fs -> fs |> tag_lst stdout
   | "tag" :: "add" :: t :: fs -> fs |> each (Name.Tag t) (tag_add stdout)
   | "tag" :: "del" :: t :: fs -> fs |> each (Name.Tag t) (tag_del stdout)
   | "title" :: "get" :: fs -> fs |> title_get stdout
   | "title" :: "set" :: t :: fs -> fs |> each (Name.Title t) (title_set stdout)
   | _ -> err 2 [ "get help with -h" ])
  |> exit
