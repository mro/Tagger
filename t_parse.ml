open Alcotest

let set_up () =
  Unix.chdir "../../"

let tc_parse () =
  let f = "2020-01-26-202853-Huhu_--_tag0_tag1.txt"
  and tags' = [ "tag0"; "tag1" ] |> List.map (fun x -> Name.Tag x) in
  let _, _, _, tags, _ = Name.parse f in
  assert (tags' = tags)

let tc_tyre_eval () =
  let f = "/a/b/2020-01-26-202853-Huhu_--_tag0_tag1.txt" in
  let p = Name.parse f in
  let f' = Tyre.eval Name.P.full p in
  assert (f = f');
  let p' = Name.tag_add p (Name.Tag "tag2") in
  let f'' = Tyre.eval Name.P.full p' in
  assert ("/a/b/2020-01-26-202853-Huhu_--_tag0_tag1_tag2.txt" = f'')

let () =
  run
    "Tagger" [
    __FILE__ , [
      "set_up"      , `Quick, set_up;
      "tc_parse"    , `Quick, tc_parse ;
      "tc_tyre_eval", `Quick, tc_tyre_eval;
    ]
  ]
